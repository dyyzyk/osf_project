$(document).ready(function() {

	search();
	popup();
	headerSlider();
	adaptiveMenu();
	newsSlider();
	twitterSlider();
	fancyBox();
	saveEmail();
	changeViewBlock();
});



function search() {
	$('.search').click(function() {
		$('.c-header__search').toggle();
		$('.c-header__dropwdown').toggle();
	})

};



function popup() {
	$('.c-header__login-form').magnificPopup();
};



function headerSlider() {
	$('.c-header__slider').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: false,
	    dots: true,
	    customPaging : function(slider, i) {
	        var title = $(slider.$slides[i]).data('title');
	        var descr = $(slider.$slides[i]).data('descr');
	        return '<div class="c-slider__dots-title"><div class="c-dots-title__item"></div><h3>'+title+'</h3><p>'+descr+'</p></div>';
	    },

	    
	});
};



function newsSlider() {
	$('.c-news__slider').slick({
		dots: false,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000, 		 
		infinite: true,
		speed: 800,
		fade: true,
		cssEase: 'linear'
	});
};

function twitterSlider() {
	$('.c-widgets__twitter-slider').slick({
		dots: false,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000, 		 
		infinite: true,
		speed: 800,
		fade: true,
		cssEase: 'linear'
	});
};


	
function fancyBox() {
	$('[data-fancybox]').fancybox({
		animationEffect : 'zoom-in-out',
		buttons : [
			'fullScreen',
			'close'
		],
	});
};



function saveEmail() {
	$.fn.serializeObject = function() {
    	var o = {};
    	var a = this.serializeArray();
    	$.each(a, function() {
        	if (o[this.name] !== undefined) {
            	if (!o[this.name].push) {
                	o[this.name] = [o[this.name]];
            	}
            	o[this.name].push(this.value || '');
        	} 	else {
            		o[this.name] = this.value || '';
        			}
    	});
    	return o;
	};

	$(function() {
		$("#imageUploadForm").submit(function(e) {
    		e.preventDefault();
  		});
    	
    	$('#uploadButton').click(function() {
    		var jsonText = JSON.stringify($('#imageUploadForm').serializeObject());
        	$.ajax({
        		type: 'POST',
        		data: jsonText,
        		crossDomain: true,
        		dataType: 'text/plain',        
        	});
   		 });
	});
};



function changeViewBlock() {
	$('#button-list').click( function() {
		$('.c-products___list').addClass('c-products__view');
		$('#button-block').removeClass('o-products__button-active');
		$('#button-list').addClass('o-products__button-active')
	});

	$('#button-block').click( function() {
		$('.c-products___list').removeClass('c-products__view');
		$('#button-list').removeClass('o-products__button-active');
		$('#button-block').addClass('o-products__button-active')
	
	}); 

};


function adaptiveMenu() {
   $('#my-menu').mmenu();
};






