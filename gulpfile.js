 var    gulp           = require('gulp'),
	    concatCSS      = require('gulp-concat-css'),
        sass           = require('gulp-sass'),
        concat         = require('gulp-concat'),
	    rename         = require('gulp-rename'),
        notify         = require('gulp-notify'),
        autoprefixer   = require('gulp-autoprefixer'),
        livereload     = require('gulp-livereload'),
        connect        = require('gulp-connect'),
        // uncss       = require('gulp-uncss'), 
        minifyCSS      = require('gulp-minify-css'),
        uglify         = require('gulp-uglify');


// server connect
gulp.task('connect', function (){
  connect.server ({
    root: 'app',
    livereload: true
  }); 
});


// sass
gulp.task('sass', function() {
    return gulp.src('app/sass/**/*.sass')
    .pipe(sass({outputStyle: 'compressed'}).on("error", notify.onError()))
    .pipe(rename({suffix: '.min', prefix : ''}))
    .pipe(autoprefixer(['last 15 versions']))
    // .pipe(cleanCSS())          // remove all unused css's classses 
    .pipe(gulp.dest('app/css'))
    .pipe(notify("Done!")) 
    .pipe(connect.reload());
});


// js
gulp.task('common-js', function() {
    return gulp.src([
        'app/js/common.js',
        ])
    .pipe(concat('common.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('app/js'))
    .pipe(notify("Done!")) 
    .pipe(connect.reload());

});

gulp.task('js', ['common-js'], function() {
    return gulp.src([
        'app/libs/jquery/dist/jquery.min.js',
        'app/libs/Magnific-Popup/jquery.magnific-popup.js',
        'app/libs/slick/slick.min.js',
        'app/libs/fancybox-master/jquery.fancybox.min.js',
        'app/libs/mmenu/jquery.mmenu.js',
        'app/js/common.min.js', // always at the end
        ])
    .pipe(concat('scripts.min.js'))
    // .pipe(uglify())          // minify all js
    .pipe(gulp.dest('app/js'))
    .pipe(notify("Done!")) 
    .pipe(connect.reload());
});



// html
gulp.task('html',function () {
    gulp.src('./app/*.html')
        .pipe(notify("Done!")) 
        .pipe(connect.reload());
}) 



//watch
gulp.task('watch', function () {
	gulp.watch('app/sass/*.sass', ['sass'])
    gulp.watch('app/*.html', ['html'])
    gulp.watch('app/js/*.js', ['js'])
})



// default
gulp.task('default', ['connect', 'html', 'sass', 'js', 'watch']);